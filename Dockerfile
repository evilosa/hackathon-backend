FROM node:12-alpine

RUN npm install yarn -g

WORKDIR /app
COPY ./config/database.js /app/config/
COPY package.json yarn.lock /app/

RUN apk add --no-cache --virtual .gyp python make g++ \
    && yarn \
    && apk del .gyp

COPY . /app

EXPOSE 80

CMD (yarn run db:create || true) && (yarn run db:migrate || true) && yarn run start
