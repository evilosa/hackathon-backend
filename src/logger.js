const settings = require('../config/settings')

const log = (color, message) => {
  console.log(color, `[${currentTime()}] [Worker - ${process.pid}]: ${message}`)
}

const epic = message => log('\x1b[35m%s\x1b[0m', message)
const error = message => log('\x1b[31m%s\x1b[0m', message)
const warn = message => log('\x1b[33m%s\x1b[0m', message)
const info = message => log('\x1b[32m%s\x1b[0m', message)

const emptyLine = () => {
  console.log('\n')
}

const currentTime = () => {
  const d = new Date()
  const milliseconds = `${
    d.getMilliseconds().toString().length === 1
      ? '00'
      : d.getMilliseconds().toString().length === 2
        ? '0'
        : ''
  }${d.getMilliseconds()}`
  const seconds = d.getSeconds().toString().length === 1 ? '0' + d.getSeconds() : d.getSeconds()
  const minutes = d.getMinutes().toString().length === 1 ? '0' + d.getMinutes() : d.getMinutes()
  const hours = d.getHours().toString().length === 1 ? '0' + d.getHours() : d.getHours()
  const months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
  return `${d.getDate()}-${
    months[d.getMonth()]
  }-${d.getFullYear()} ${hours}:${minutes}:${seconds}:${milliseconds}`
}

module.exports = {
  epic: message => (settings.isTest ? {} : epic(message)),
  error: message => (settings.isTest ? {} : error(message)),
  warning: message => (settings.isTest ? {} : warn(message)),
  info: message => (settings.isTest ? {} : info(message)),
  emptyLine: () => (settings.isTest ? {} : emptyLine())
}
