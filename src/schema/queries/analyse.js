const { GraphQLNonNull, GraphQLList, GraphQLString } = require('graphql')
const { Company } = require('@models')
const CompanyAnalyseType = require('@schema/types/companyAnalyseType')

const checkCompanyAddressesAffilationAsync = require('../../../src/analyse/checkCompanyAddressesAffilation')
const checkCompanyPersonAffilationAsync = require('../../../src/analyse/checkCompanyPeopleAffilation')
const getPeopleRelativesAsync = require('../../../src/analyse/getPeopleRelatives')
const getWrongOkvedMarkersAsync = require('../../../src/analyse/getWrongOkvedMarkers')

const addMarkersToResult = (result, source) => {
  Object.keys(source).forEach(key => {
    result[key] = {
      ...result[key],
      markers: [...result[key].markers, ...source[key]]
    }
  })
}

const resolve = async (parentValue, { companies }) => {
  const listOfCompanies = await Company.findAll({ where: { id: companies } })

  const result = listOfCompanies.reduce((accum, company) => {
    accum[company.id] = {
      company,
      markers: []
    }
    return accum
  }, {})

  addMarkersToResult(result, await checkCompanyAddressesAffilationAsync(companies))
  addMarkersToResult(result, await checkCompanyPersonAffilationAsync(companies))
  addMarkersToResult(result, await getPeopleRelativesAsync(companies))
  addMarkersToResult(result, await getWrongOkvedMarkersAsync(companies))
  return Object.keys(result).map(key => result[key])
}

const analyse = {
  type: new GraphQLList(CompanyAnalyseType),
  args: {
    companies: { type: new GraphQLNonNull(new GraphQLList(GraphQLString)) }
  },
  resolve
}

module.exports = analyse
module.exports.getAnalyseAsync = resolve
