const graphql = require('graphql')

const { GraphQLObjectType, GraphQLID, GraphQLString } = graphql

const AddressType = new GraphQLObjectType({
  name: 'Address',
  fields: () => ({
    id: { type: GraphQLID },
    coutry: { type: GraphQLString },
    city: { type: GraphQLString },
    street: { type: GraphQLString },
    building: { type: GraphQLString },
    place: { type: GraphQLString }
  })
})

module.exports = AddressType
