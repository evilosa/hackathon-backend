const graphql = require('graphql')
const bcrypt = require('bcrypt')
const jsonwebtoken = require('jsonwebtoken')
const settings = require('../../../config/settings')

const { GraphQLNonNull, GraphQLString } = graphql
const { User } = require('../../../models')

const resolve = async (_parent, { email, password }) => {
  const user = await User.findOne({ where: { email } })

  if (!user) {
    throw new Error('Wrong email or password!')
  }

  const valid = await bcrypt.compare(password, user.password)

  if (!valid) {
    throw new Error('Wrong email or password!')
  }

  // return json web token
  return jsonwebtoken.sign(
    { id: user.id, email: user.email },
    settings.jwtSecret,
    { expiresIn: '1d' }
  )
}

const login = {
  type: GraphQLString,
  args: {
    email: { type: new GraphQLNonNull(GraphQLString) },
    password: { type: new GraphQLNonNull(GraphQLString) }
  },
  resolve
}

module.exports = login
module.exports.loginAsync = resolve
