const graphql = require('graphql')
const settings = require('@config/settings')
const bcrypt = require('bcrypt')
const jsonwebtoken = require('jsonwebtoken')
const { User, Role } = require('@models')

const { GraphQLNonNull, GraphQLString } = graphql

const resolve = async (parentValue, { email, password }) => {
  // Guard part
  if (!password) {
    throw new Error('notNull Violation: User.password cannot be null')
  }

  const emailReservedUser = await User.findOne({ where: { email } })
  if (emailReservedUser) {
    throw new Error('User with the same email already registered!')
  }

  const userRole = await Role.findOrCreate({
    where: { name: 'user' },
    defaults: { name: 'user' }
  }).spread(async (role, created) => {
    if (created) {
      await role.save()
    }

    return role
  })

  // const userRole = await Role.findOne({ where: { name: 'user' } })

  const user = await User.create({
    email,
    password: await bcrypt.hash(password, 10)
  })

  user.addRole(userRole)

  // return json web token
  return jsonwebtoken.sign(
    { id: user.id, email: user.email },
    settings.jwtSecret,
    { expiresIn: '1y' }
  )
}

const signUp = {
  type: GraphQLString,
  args: {
    email: { type: new GraphQLNonNull(GraphQLString) },
    password: { type: new GraphQLNonNull(GraphQLString) }
  },
  resolve
}

module.exports = signUp
module.exports.signUpAsync = resolve
