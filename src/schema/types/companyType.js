const graphql = require('graphql')

const { GraphQLObjectType, GraphQLID, GraphQLString, GraphQLBoolean } = graphql

const CompanyType = new GraphQLObjectType({
  name: 'Company',
  fields: () => ({
    id: { type: GraphQLID },
    taxNumber: { type: GraphQLString },
    name: { type: GraphQLString },
    fullName: { type: GraphQLString },
    description: { type: GraphQLString },
    isBlocked: { type: GraphQLBoolean }
  })
})

module.exports = CompanyType
