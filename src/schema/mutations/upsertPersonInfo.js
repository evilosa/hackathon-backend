const graphql = require('graphql')

const { GraphQLNonNull, GraphQLString } = graphql
const { Person } = require('../../../models')

const resolve = async (_parent, args) => {
  const { taxNumber } = args

  const person = await Person.findOrCreate({
    where: { taxNumber },
    defaults: { ...args }
  }).spread((person, created) => created ? person : person.update(args))

  await person.save()
  return person.id
}

const upsertPersonInfo = {
  type: GraphQLString,
  args: {
    taxNumber: { type: new GraphQLNonNull(GraphQLString) },
    name: { type: new GraphQLNonNull(GraphQLString) },
    middleName: { type: new GraphQLNonNull(GraphQLString) },
    surname: { type: new GraphQLNonNull(GraphQLString) }
  },
  resolve
}

module.exports = upsertPersonInfo
module.exports.upsertPersonInfoAsync = resolve
