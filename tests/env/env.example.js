const {
  NODE_ENV = 'test',
  JWT_SECRET = 'some-long-jwt-secret-for-tests'
} = process.env

const ENV = {
  NODE_ENV,
  JWT_SECRET
}

process.env = ENV

module.exports.ENV = ENV
