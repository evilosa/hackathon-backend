'use strict'

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      { tableName: 'CompaniesAddresses', schema: 'public' },
      [
        { companyId: 1, addressId: 11 },
        { companyId: 2, addressId: 10 },
        { companyId: 3, addressId: 9 },
        { companyId: 4, addressId: 8 },
        { companyId: 5, addressId: 7 },
        { companyId: 6, addressId: 6 },
        { companyId: 7, addressId: 5 },
        { companyId: 8, addressId: 4 },
        { companyId: 9, addressId: 3 },
        { companyId: 10, addressId: 2 },
        { companyId: 11, addressId: 1 },
        { companyId: 12, addressId: 11 }
      ],
      {}
    )
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('CompaniesAddresses', null, {})
  }
}
