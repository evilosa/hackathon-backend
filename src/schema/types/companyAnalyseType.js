const graphql = require('graphql')

const { GraphQLList, GraphQLObjectType } = graphql
const CompanyType = require('./companyType')
const MarkerType = require('./markerType')

const CompanyAnalyseType = new GraphQLObjectType({
  name: 'CompanyAnalyse',
  fields: () => ({
    company: { type: CompanyType },
    markers: { type: new GraphQLList(MarkerType) }
  })
})

module.exports = CompanyAnalyseType
