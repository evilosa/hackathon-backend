'use strict'

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      { tableName: 'PeopleAddresses', schema: 'public' },
      [
        { personId: 1, addressId: 1 },
        { personId: 2, addressId: 2 },
        { personId: 3, addressId: 3 },
        { personId: 4, addressId: 4 },
        { personId: 5, addressId: 5 },
        { personId: 6, addressId: 6 },
        { personId: 7, addressId: 7 },
        { personId: 8, addressId: 8 },
        { personId: 9, addressId: 4 },
        { personId: 10, addressId: 3 }

      ],
      {}
    )
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('PeopleAddresses', null, {})
  }
}
