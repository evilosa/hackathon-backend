const { Company, Person } = require('@models')

const getPeopleRelatives = async (companies) => {
  const listOfCompanies = await Company.findAll({ where: { id: companies }, include: { model: Person, as: 'people', foreignKey: 'personId' }, orderBy: 'id' })

  const affilated = {}

  for (let i = 0; i < listOfCompanies.length; i++) {
    const companyId = listOfCompanies[i].id

    let relatives = []

    for (let j = 0; j < listOfCompanies.length; j++) {
      if (i !== j) {
        const intersection = listOfCompanies[i].people.filter(person => listOfCompanies[j].people.map(addr => addr.id).includes(person.id))
        if (intersection.length > 0) {
          if (i % 3 === 0) {
            relatives.push(`Поставщика ${j + 1}`)
          }
        }
      }
    }

    affilated[companyId] = relatives.map(affilatedId => ({
      marker: 'Родственные связи',
      level: 'warning',
      result: `Является родственником учредителя ${affilatedId}`
    })) || []
  }

  return affilated
}

module.exports = getPeopleRelatives
