'use strict'

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      { tableName: 'TradeOffers', schema: 'public' },
      [
        { tradeId: 1, companyId: 1, sum: 125003, createdAt: new Date(2019, 5, 20), updatedAt: new Date(2019, 5, 20) },
        { tradeId: 1, companyId: 2, sum: 178543, createdAt: new Date(2019, 5, 20), updatedAt: new Date(2019, 5, 20) },
        { tradeId: 1, companyId: 3, sum: 145983, createdAt: new Date(2019, 5, 20), updatedAt: new Date(2019, 5, 20) },
        { tradeId: 1, companyId: 4, sum: 245000, createdAt: new Date(2019, 5, 20), updatedAt: new Date(2019, 5, 20) },
        { tradeId: 1, companyId: 5, sum: 143223, createdAt: new Date(2019, 5, 20), updatedAt: new Date(2019, 5, 20) },
        { tradeId: 1, companyId: 6, sum: 432098, createdAt: new Date(2019, 5, 20), updatedAt: new Date(2019, 5, 20) },
        { tradeId: 1, companyId: 7, sum: 194876, createdAt: new Date(2019, 5, 20), updatedAt: new Date(2019, 5, 20) },
        { tradeId: 1, companyId: 8, sum: 183234, createdAt: new Date(2019, 5, 20), updatedAt: new Date(2019, 5, 20) },
        { tradeId: 1, companyId: 9, sum: 243932, createdAt: new Date(2019, 5, 20), updatedAt: new Date(2019, 5, 20) },
        { tradeId: 1, companyId: 10, sum: 145432, createdAt: new Date(2019, 5, 20), updatedAt: new Date(2019, 5, 20) },
        { tradeId: 1, companyId: 11, sum: 165434, createdAt: new Date(2019, 5, 20), updatedAt: new Date(2019, 5, 20) },
        { tradeId: 1, companyId: 12, sum: 187432, createdAt: new Date(2019, 5, 20), updatedAt: new Date(2019, 5, 20) },
        { tradeId: 2, companyId: 1, sum: 105000, createdAt: new Date(2019, 5, 20), updatedAt: new Date(2019, 5, 20) },
        { tradeId: 2, companyId: 2, sum: 223003, createdAt: new Date(2019, 5, 20), updatedAt: new Date(2019, 5, 20) },
        { tradeId: 2, companyId: 3, sum: 400000, createdAt: new Date(2019, 5, 20), updatedAt: new Date(2019, 5, 20) },
        { tradeId: 2, companyId: 4, sum: 175500, createdAt: new Date(2019, 5, 20), updatedAt: new Date(2019, 5, 20) },
        { tradeId: 2, companyId: 5, sum: 1000000, createdAt: new Date(2019, 5, 20), updatedAt: new Date(2019, 5, 20) },
        { tradeId: 2, companyId: 6, sum: 125454, createdAt: new Date(2019, 5, 20), updatedAt: new Date(2019, 5, 20) },
        { tradeId: 2, companyId: 7, sum: 456750, createdAt: new Date(2019, 5, 20), updatedAt: new Date(2019, 5, 20) },
        { tradeId: 2, companyId: 8, sum: 433020, createdAt: new Date(2019, 5, 20), updatedAt: new Date(2019, 5, 20) },
        { tradeId: 2, companyId: 9, sum: 600450, createdAt: new Date(2019, 5, 20), updatedAt: new Date(2019, 5, 20) },
        { tradeId: 2, companyId: 10, sum: 587432, createdAt: new Date(2019, 5, 20), updatedAt: new Date(2019, 5, 20) },
        { tradeId: 2, companyId: 11, sum: 450530, createdAt: new Date(2019, 5, 20), updatedAt: new Date(2019, 5, 20) },
        { tradeId: 2, companyId: 12, sum: 302321, createdAt: new Date(2019, 5, 20), updatedAt: new Date(2019, 5, 20) },
        { tradeId: 3, companyId: 1, sum: 195423, createdAt: new Date(2019, 5, 20), updatedAt: new Date(2019, 5, 20) },
        { tradeId: 3, companyId: 2, sum: 232322, createdAt: new Date(2019, 5, 20), updatedAt: new Date(2019, 5, 20) },
        { tradeId: 3, companyId: 3, sum: 406000, createdAt: new Date(2019, 5, 20), updatedAt: new Date(2019, 5, 20) },
        { tradeId: 3, companyId: 4, sum: 378020, createdAt: new Date(2019, 5, 20), updatedAt: new Date(2019, 5, 20) },
        { tradeId: 3, companyId: 5, sum: 456000, createdAt: new Date(2019, 5, 20), updatedAt: new Date(2019, 5, 20) },
        { tradeId: 3, companyId: 6, sum: 465523, createdAt: new Date(2019, 5, 20), updatedAt: new Date(2019, 5, 20) },
        { tradeId: 3, companyId: 7, sum: 385432, createdAt: new Date(2019, 5, 20), updatedAt: new Date(2019, 5, 20) },
        { tradeId: 3, companyId: 8, sum: 894321, createdAt: new Date(2019, 5, 20), updatedAt: new Date(2019, 5, 20) },
        { tradeId: 3, companyId: 9, sum: 134000, createdAt: new Date(2019, 5, 20), updatedAt: new Date(2019, 5, 20) },
        { tradeId: 3, companyId: 10, sum: 430020, createdAt: new Date(2019, 5, 20), updatedAt: new Date(2019, 5, 20) },
        { tradeId: 3, companyId: 11, sum: 400000, createdAt: new Date(2019, 5, 20), updatedAt: new Date(2019, 5, 20) },
        { tradeId: 3, companyId: 12, sum: 199453, createdAt: new Date(2019, 5, 20), updatedAt: new Date(2019, 5, 20) },
        { tradeId: 4, companyId: 1, sum: 1230000, createdAt: new Date(2019, 5, 20), updatedAt: new Date(2019, 5, 20) },
        { tradeId: 4, companyId: 2, sum: 1430000, createdAt: new Date(2019, 5, 20), updatedAt: new Date(2019, 5, 20) },
        { tradeId: 4, companyId: 3, sum: 2540000, createdAt: new Date(2019, 5, 20), updatedAt: new Date(2019, 5, 20) },
        { tradeId: 4, companyId: 4, sum: 9870000, createdAt: new Date(2019, 5, 20), updatedAt: new Date(2019, 5, 20) },
        { tradeId: 4, companyId: 5, sum: 1020000, createdAt: new Date(2019, 5, 20), updatedAt: new Date(2019, 5, 20) },
        { tradeId: 4, companyId: 6, sum: 1123500, createdAt: new Date(2019, 5, 20), updatedAt: new Date(2019, 5, 20) },
        { tradeId: 4, companyId: 7, sum: 1634290, createdAt: new Date(2019, 5, 20), updatedAt: new Date(2019, 5, 20) },
        { tradeId: 4, companyId: 8, sum: 2450000, createdAt: new Date(2019, 5, 20), updatedAt: new Date(2019, 5, 20) },
        { tradeId: 4, companyId: 9, sum: 1923000, createdAt: new Date(2019, 5, 20), updatedAt: new Date(2019, 5, 20) },
        { tradeId: 4, companyId: 10, sum: 4320000, createdAt: new Date(2019, 5, 20), updatedAt: new Date(2019, 5, 20) },
        { tradeId: 4, companyId: 11, sum: 3863000, createdAt: new Date(2019, 5, 20), updatedAt: new Date(2019, 5, 20) },
        { tradeId: 4, companyId: 12, sum: 2320000, createdAt: new Date(2019, 5, 20), updatedAt: new Date(2019, 5, 20) },
        { tradeId: 5, companyId: 1, sum: 78543, createdAt: new Date(2019, 5, 20), updatedAt: new Date(2019, 5, 20) },
        { tradeId: 5, companyId: 2, sum: 120000, createdAt: new Date(2019, 5, 20), updatedAt: new Date(2019, 5, 20) },
        { tradeId: 5, companyId: 3, sum: 165000, createdAt: new Date(2019, 5, 20), updatedAt: new Date(2019, 5, 20) },
        { tradeId: 5, companyId: 4, sum: 230000, createdAt: new Date(2019, 5, 20), updatedAt: new Date(2019, 5, 20) },
        { tradeId: 5, companyId: 5, sum: 321594, createdAt: new Date(2019, 5, 20), updatedAt: new Date(2019, 5, 20) },
        { tradeId: 5, companyId: 6, sum: 128300, createdAt: new Date(2019, 5, 20), updatedAt: new Date(2019, 5, 20) },
        { tradeId: 5, companyId: 7, sum: 99000, createdAt: new Date(2019, 5, 20), updatedAt: new Date(2019, 5, 20) },
        { tradeId: 5, companyId: 8, sum: 200100, createdAt: new Date(2019, 5, 20), updatedAt: new Date(2019, 5, 20) },
        { tradeId: 5, companyId: 9, sum: 300000, createdAt: new Date(2019, 5, 20), updatedAt: new Date(2019, 5, 20) },
        { tradeId: 5, companyId: 10, sum: 156000, createdAt: new Date(2019, 5, 20), updatedAt: new Date(2019, 5, 20) },
        { tradeId: 5, companyId: 11, sum: 100330, createdAt: new Date(2019, 5, 20), updatedAt: new Date(2019, 5, 20) },
        { tradeId: 5, companyId: 12, sum: 143000, createdAt: new Date(2019, 5, 20), updatedAt: new Date(2019, 5, 20) }
      ],
      {}
    )
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('TradeOffers', null, {})
  }
}
