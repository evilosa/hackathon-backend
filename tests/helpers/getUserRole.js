const { Role } = require('@models')

const getUserRoleAsync = async name => {
  return Role.findOrCreate({
    where: { name },
    defaults: { name }
  }).spread(async (role, created) => {
    if (created) {
      await role.save()
    }

    return role
  })
}

module.exports.getUserRoleAsync = getUserRoleAsync
