const graphql = require('graphql')
const { GraphQLObjectType } = graphql

const analyse = require('./analyse')
const getMe = require('./getMe')
const trades = require('./trades')

const RootQuery = new GraphQLObjectType({
  name: 'RootQueryType',
  fields: () => ({
    me: getMe,
    analyse,
    trades
  })
})

module.exports = RootQuery
