const graphql = require('graphql')

const { GraphQLNonNull, GraphQLString, GraphQLBoolean } = graphql
const { Company } = require('../../../models')

const resolve = async (_parent, args) => {
  const { taxNumber } = args

  const company = await Company.findOrCreate({
    where: { taxNumber },
    defaults: { ...args }
  }).spread((company, created) => created ? company : company.update(args))

  await company.save()
  return company.id
}

const upsertCompanyInfo = {
  type: GraphQLString,
  args: {
    taxNumber: { type: new GraphQLNonNull(GraphQLString) },
    name: { type: new GraphQLNonNull(GraphQLString) },
    fullName: { type: new GraphQLNonNull(GraphQLString) },
    description: { type: GraphQLString },
    isBlocked: { type: GraphQLBoolean }
  },
  resolve
}

module.exports = upsertCompanyInfo
module.exports.upsertCompanyInfoAsync = resolve
