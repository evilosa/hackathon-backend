const graphql = require('graphql')

const { GraphQLObjectType, GraphQLID, GraphQLFloat } = graphql
const CompanyType = require('./companyType')

const TradeOfferType = new GraphQLObjectType({
  name: 'TradeOffer',
  fields: () => ({
    id: { type: GraphQLID },
    sum: { type: GraphQLFloat },
    company: { type: CompanyType }
  })
})

module.exports = TradeOfferType
