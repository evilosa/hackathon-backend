'use strict'

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      { tableName: 'CompaniesPeople', schema: 'public' },
      [
        { personId: 1, companyId: 1 },
        { personId: 3, companyId: 1 },
        { personId: 2, companyId: 2 },
        { personId: 6, companyId: 2 },
        { personId: 3, companyId: 3 },
        { personId: 4, companyId: 4 },
        { personId: 2, companyId: 5 },
        { personId: 4, companyId: 6 },
        { personId: 5, companyId: 7 },
        { personId: 8, companyId: 8 },
        { personId: 9, companyId: 9 },
        { personId: 10, companyId: 10 },
        { personId: 7, companyId: 11 },
        { personId: 1, companyId: 12 }
      ],
      {}
    )
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('CompaniesPeople', null, {})
  }
}
