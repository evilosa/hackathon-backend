'use strict'
module.exports = (sequelize, DataTypes) => {
  const Person = sequelize.define('Person', {
    taxNumber: DataTypes.STRING,
    name: DataTypes.STRING,
    middleName: DataTypes.STRING,
    surname: DataTypes.STRING
  }, {
    timestamps: false,
    schema: 'public'
  })
  Person.associate = (models) => {
    // associations can be defined here
    Person.belongsToMany(models.Company, { as: 'companies', through: 'CompaniesPeople', foreignKey: 'personId' })
    Person.belongsToMany(models.Address, { as: 'addresses', through: 'PeopleAddresses', foreignKey: 'personId' })
    Person.belongsToMany(models.Person, { as: 'relativeTo', through: 'PeopleRelatives', foreignKey: 'personId' })
    Person.belongsToMany(models.Person, { as: 'myRelatives', through: 'PeopleRelatives', foreignKey: 'relativeId' })
    Person.hasMany(models.PersonConviction, { as: 'convictions', foreignKey: 'personId' })
  }
  return Person
}
