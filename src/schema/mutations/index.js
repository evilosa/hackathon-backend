const graphql = require('graphql')
const { GraphQLObjectType } = graphql

const addCompanyAddress = require('./addCompanyAddress')
const addCompanyConviction = require('./addCompanyConviction')
const addPersonAddress = require('./addPersonAddress')
const addPersonConviction = require('./addPersonConviction')
const addPersonRelatives = require('./addPersonRelatives')
const addTrade = require('./addTrade')
const upsertAddressInfo = require('./upsertAddressInfo')
const upsertCompanyInfo = require('./upsertCompanyInfo')
const upsertPersonInfo = require('./upsertPersonInfo')

const signUp = require('./signUp')
const login = require('./login')

const mutations = new GraphQLObjectType({
  name: 'Mutation',
  fields: {
    addCompanyAddress,
    addCompanyConviction,
    addPersonAddress,
    addPersonConviction,
    addPersonRelatives,
    addTrade,
    upsertAddressInfo,
    upsertCompanyInfo,
    upsertPersonInfo,
    signUp,
    login
  }
})

module.exports = mutations
