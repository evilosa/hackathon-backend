const graphql = require('graphql')

const { GraphQLNonNull, GraphQLString } = graphql
const { Address } = require('../../../models')
const AddressType = require('../types/addressType')

const resolve = async (_parent, args) => {
  const address = await Address.findOrCreate({
    where: args,
    defaults: args
  }).spread(address => address)

  await address.save()
  return address
}

const upsertAddressInfo = {
  type: AddressType,
  args: {
    country: { type: new GraphQLNonNull(GraphQLString) },
    city: { type: new GraphQLNonNull(GraphQLString) },
    street: { type: new GraphQLNonNull(GraphQLString) },
    building: { type: new GraphQLNonNull(GraphQLString) },
    place: { type: new GraphQLNonNull(GraphQLString) }
  },
  resolve
}

module.exports = upsertAddressInfo
module.exports.upsertAddressInfoAsync = resolve
