'use strict'

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      { tableName: 'Companies', schema: 'public' },
      [{
        id: 1,
        taxNumber: '2303000001',
        name: 'Поставщик 1',
        fullName: 'ООО Поставщик 1',
        description: 'Комания занимается поставкой сельскохозяйственной техники',
        isBlocked: false,
        openDate: new Date(2017, 1, 10)
      },
      {
        id: 2,
        taxNumber: '2303000002',
        name: 'Поставщик 2',
        fullName: 'ООО Поставщик 2',
        description: 'Комания занимается поставкой компьютерной техники',
        isBlocked: false,
        openDate: new Date(2014, 1, 10)
      },
      {
        id: 3,
        taxNumber: '2303000003',
        name: 'Поставщик 3',
        fullName: 'ООО Поставщик 3',
        description: 'Комания занимается поставкой компьютерной техники',
        isBlocked: false,
        openDate: new Date(2015, 1, 10)
      },
      {
        id: 4,
        taxNumber: '2303000004',
        name: 'Поставщик 4',
        fullName: 'ООО Поставщик 4',
        description: 'Комания занимается поставкой одежды',
        isBlocked: false,
        openDate: new Date(2019, 5, 10)
      },
      {
        id: 5,
        taxNumber: '2303000005',
        name: 'Поставщик 5',
        fullName: 'ООО Поставщик 5',
        description: 'Комания занимается поставкой бытовой техники',
        isBlocked: false,
        openDate: new Date(2013, 1, 10)
      },
      {
        id: 6,
        taxNumber: '2303000006',
        name: 'Поставщик 6',
        fullName: 'ООО Поставщик 6',
        description: 'Комания занимается поставкой компьютерной техники',
        isBlocked: false,
        openDate: new Date(2019, 1, 10)
      },
      {
        id: 7,
        taxNumber: '2303000007',
        name: 'Поставщик 7',
        fullName: 'ООО Поставщик 7',
        description: 'Комания занимается поставкой компьютерной техники',
        isBlocked: false,
        openDate: new Date(2017, 1, 10)
      },
      {
        id: 8,
        taxNumber: '2303000008',
        name: 'Поставщик 8',
        fullName: 'ООО Поставщик 8',
        description: 'Комания занимается поставкой компьютерной техники',
        isBlocked: false,
        openDate: new Date(2014, 1, 10)
      },
      {
        id: 9,
        taxNumber: '2303000009',
        name: 'Поставщик 9',
        fullName: 'ООО Поставщик 9',
        description: 'Комания занимается поставкой инертных материалов',
        isBlocked: false,
        openDate: new Date(2011, 1, 10)
      },
      {
        id: 10,
        taxNumber: '2303000010',
        name: 'Поставщик 10',
        fullName: 'ООО Поставщик 10',
        description: 'Комания занимается поставкой компьютерной техники',
        isBlocked: false,
        openDate: new Date(2008, 1, 10)
      },
      {
        id: 11,
        taxNumber: '2303000011',
        name: 'Поставщик 11',
        fullName: 'ООО Поставщик 11',
        description: 'Комания занимается поставкой горюче-смазочных материалов',
        isBlocked: false,
        openDate: new Date(2016, 1, 10)
      },
      {
        id: 12,
        taxNumber: '2303000012',
        name: 'Поставщик 12',
        fullName: 'ООО Поставщик 12',
        description: 'Комания занимается поставкой компьютерной техники',
        isBlocked: false,
        openDate: new Date(2017, 1, 10)
      }
      ],
      {}
    )
  },
  down: async (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Companies', null, {})
  }
}
