const dialect = 'postgres'

const sharedSettings = {
  host: process.env.POSTGRES_HOST || 'postgres',
  port: process.env.POSTGRES_PORT || 5432,
  username: process.env.POSTGRES_USER || 'evilosa',
  password: process.env.POSTGRES_PASS || 'detteER1337cool',
  database: process.env.POSTGRES_DB,
  dialect,
  logging: process.env.SQL_LOGGING || false,
  pool: {
    max: process.env.POOL_MAX || 20,
    min: process.env.POOL_MIN || 0,
    acquire: process.env.POOL_TIMEOUT || 30000,
    idle: process.env.POOL_IDLE || 10000,
    evict: process.env.POOL_EVICT || 9990
  },
  seederStorage: 'sequelize'
}

module.exports = {
  development: { ...sharedSettings, ...{ database: sharedSettings.database || 'dragonites_dev' } },
  test: { ...sharedSettings, ...{ database: sharedSettings.database || 'dragonites_test' } },
  production: sharedSettings
}
