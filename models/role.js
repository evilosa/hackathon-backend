'use strict'
module.exports = (sequelize, DataTypes) => {
  const Role = sequelize.define('Role', {
    name: DataTypes.STRING
  }, {
    timestamps: false,
    schema: 'public'
  })
  Role.associate = models => {
    // associations can be defined here
    Role.belongsToMany(models.User, { as: 'users', through: 'UserRoles', foreignKey: 'roleId' })
  }
  return Role
}
