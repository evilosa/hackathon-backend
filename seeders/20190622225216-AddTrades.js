'use strict'

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      { tableName: 'Trades', schema: 'public' },
      [{
        id: 1,
        title: 'Ремонт дороги с. Старотутуево',
        description: 'Ремонт дороги в с. Старотураево по улице Восточная муниципального района Ермекеевский район Республики Башкортостан',
        createdAt: new Date(2019, 5, 20),
        updatedAt: new Date(2019, 5, 20)
      },
      {
        id: 2,
        title: 'Ремонт дороги с. Ачан',
        description: 'Выполнение работ по текущему ремонту внутрипоселенческих автомобильных дорог на территории сельского поселения "Село Ачан" Амурского муниципального района Хабаровского края в 2019 году',
        createdAt: new Date(2019, 5, 20),
        updatedAt: new Date(2019, 5, 20)
      },
      {
        id: 3,
        title: 'Капремонт дворовых территорий',
        description: 'Капитальный ремонт дворовых территорий МО "Томаринский городской округ" с. Красногорск, ул. Победы 9',
        createdAt: new Date(2019, 5, 20),
        updatedAt: new Date(2019, 5, 20)
      },
      {
        id: 4,
        title: 'Ремонт ГБОУ РШИ №32',
        description: 'Выполнение работ по текущему ремонту в ГБОУ РШИ №32',
        createdAt: new Date(2019, 5, 20),
        updatedAt: new Date(2019, 5, 20)
      },
      {
        id: 5,
        title: 'Поставка медицинского оборудования',
        description: 'Поставка аппарата ультра-звукового исследования в ЦРБ Белореченского района',
        createdAt: new Date(2019, 5, 20),
        updatedAt: new Date(2019, 5, 20)
      }
      ],
      {}
    )
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Trades', null, {})
  }
}
