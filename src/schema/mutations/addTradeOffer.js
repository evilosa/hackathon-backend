const graphql = require('graphql')

const { GraphQLNonNull, GraphQLString, GraphQLFloat } = graphql
const { Company, TradeOffer } = require('../../../models')

const resolve = async (_parent, { taxNumber, sum, tradeId }) => {
  const company = await Company.findOne({ where: { taxNumber } })
  if (company) {
    const tradeOffer = await TradeOffer.create({ tradeId, companyId: company.id, sum })
    return tradeOffer.id
  } else {
    throw new Error(`Company with tax number ${taxNumber} doesn't registered in system!`)
  }
}

const addTrade = {
  type: GraphQLString,
  args: {
    tradeId: { type: new GraphQLNonNull(GraphQLString) },
    taxNumber: { type: new GraphQLNonNull(GraphQLString) },
    sum: { type: new GraphQLNonNull(GraphQLFloat) }
  },
  resolve
}

module.exports = addTrade
module.exports.addTradeAsync = resolve
