'use strict'

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      { tableName: 'Addresses', schema: 'public' },
      [{
        id: 1,
        country: 'Российская Федерация',
        city: 'Краснодар',
        street: 'Ставропольская',
        building: '125',
        place: ''
      },
      {
        id: 2,
        country: 'Российская Федерация',
        city: 'Краснодар',
        street: 'Володи Головатого',
        building: '15',
        place: '24'
      },
      {
        id: 3,
        country: 'Российская Федерация',
        city: 'Краснодар',
        street: 'Гудимы',
        building: '432',
        place: ''
      },
      {
        id: 4,
        country: 'Российская Федерация',
        city: 'Краснодар',
        street: 'Нескучный сад',
        building: '19',
        place: ''
      },
      {
        id: 5,
        country: 'Российская Федерация',
        city: 'Белореченск',
        street: 'Ленина',
        building: '125',
        place: ''
      },
      {
        id: 6,
        country: 'Российская Федерация',
        city: 'Краснодар',
        street: 'Интернациональная',
        building: '27',
        place: ''
      },
      {
        id: 7,
        country: 'Российская Федерация',
        city: 'Краснодар',
        street: 'Фестивальная',
        building: '54',
        place: ''
      },
      {
        id: 8,
        country: 'Российская Федерация',
        city: 'Краснодар',
        street: 'Красная',
        building: '15',
        place: '34'
      },
      {
        id: 9,
        country: 'Российская Федерация',
        city: 'Краснодар',
        street: 'Гагарина',
        building: '112',
        place: ''
      },
      {
        id: 10,
        country: 'Российская Федерация',
        city: 'Краснодар',
        street: 'Шаляпина',
        building: '10',
        place: '76'
      },
      {
        id: 11,
        country: 'Российская Федерация',
        city: 'Краснодар',
        street: 'Карасунская',
        building: '192',
        place: '63'
      }
      ],
      {}
    )
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Addresses', null, {})
  }
}
