'use strict'
module.exports = (sequelize, DataTypes) => {
  const CompanyConviction = sequelize.define('CompanyConviction', {
    description: DataTypes.STRING
  }, {
    schema: 'public'
  })
  CompanyConviction.associate = (models) => {
    // associations can be defined here
    CompanyConviction.belongsTo(models.Person, { as: 'company', foreignKey: 'companyId' })
  }
  return CompanyConviction
}
