const { User, Role } = require('@models')
const { signUpAsync } = require('@schema/mutations/signUp')

describe('Sign up mutation', () => {
  beforeAll(async () => {
    await User.destroy({ truncate: true, cascade: true })
    await Role.destroy({ truncate: true, cascade: true })
  })

  const userData = { email: 'email@email.com', password: 'password' }

  it('should create new user when called with valid data', async () => {
    await signUpAsync(null, userData)
    expect(await User.count()).toEqual(1)
  })

  it('should create a user with an encrypted password', async () => {
    const newUser = await User.findOne({ where: { email: 'email@email.com' } })
    expect(newUser.password).not.toEqual('password')
  })

  it('should create a user with a basic `user` role', async () => {
    const newUser = await User.findOne({ where: { email: 'email@email.com' } })
    expect(await newUser.getRoles().map(role => role.name)).toContain('user')
  })

  describe('throw error checks', () => {
    it('shouldn\'t create a new user with the same email', async () => {
      await expect(signUpAsync(null, userData)).rejects.toThrowError('User with the same email already registered!')
    })

    it('shouldn\'t create a new user with the invalid email', async () => {
      await expect(signUpAsync(null, {
        ...userData,
        ...{ email: 'some-email', username: 'some-user' }
      })).rejects.toThrowError('Validation error: Validation isEmail on email failed')
    })

    it('shouldn\'t create a new user with the null email', async () => {
      await expect(signUpAsync(null, {
        username: 'some-user-name',
        email: null,
        password: 'some-pass'
      })).rejects.toThrowError('notNull Violation: User.email cannot be null')
    })

    it('shouldn\'t create a new user with the null password', async () => {
      await expect(signUpAsync(null, {
        username: 'some-user-name',
        email: 'some@email.com',
        password: null
      })).rejects.toThrowError('notNull Violation: User.password cannot be null')
    })
  })
})
