const { User } = require('@models')
const UserType = require('@schema/types/userType')
const { requiresUserAccess } = require('@schema/authentication')

const resolve = async (parentValue, _, { user }) => {
  return User.findOne({ where: { id: user.id } })
}

const getMe = {
  type: UserType,
  resolve: requiresUserAccess(resolve)
}

module.exports = getMe
module.exports.getMeAsync = resolve
