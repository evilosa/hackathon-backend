require('module-alias/register')

const express = require('express')
const jwt = require('express-jwt')
const promMid = require('express-prometheus-middleware')
const cors = require('cors')
const graphqlHTTP = require('express-graphql')
const schema = require('@schema')
const settings = require('@config/settings')
const { port } = settings

const app = express()

const authMiddleware = jwt({ secret: settings.jwtSecret, credentialsRequired: false })

app.use(promMid({
  metricsPath: '/metrics',
  collectDefaultMetrics: true,
  requestDurationBuckets: [0.1, 0.5, 1, 1.5]
}))

app.use('/healthz', (req, res) => {
  res.status(200).send('ok')
})

app.use('/api/graphql', [cors(), authMiddleware], graphqlHTTP(req => ({
  schema,
  graphiql: settings.isDevelopment,
  context: {
    user: req.user
  }
})))

app.listen(port, () => {
  console.log(`Server started at http://localhost:${port}/api/graphql`)
})
