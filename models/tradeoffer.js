'use strict'
module.exports = (sequelize, DataTypes) => {
  const TradeOffer = sequelize.define('TradeOffer', {
    sum: DataTypes.FLOAT
  }, {
    schema: 'public'
  })
  TradeOffer.associate = function (models) {
    // associations can be defined here
    TradeOffer.belongsTo(models.Company, { as: 'company', foreignKey: 'companyId' })
    TradeOffer.belongsTo(models.Trade, { as: 'trade', foreignKey: 'tradeId' })
  }
  return TradeOffer
}
