'use strict'
module.exports = (sequelize, DataTypes) => {
  const CompanyFinancialState = sequelize.define('CompanyFinancialState', {
    state: DataTypes.STRING
  }, {
    schema: 'public'
  })
  CompanyFinancialState.associate = (models) => {
    // associations can be defined here
    CompanyFinancialState.belongsTo(models.Person, { as: 'company', foreignKey: 'companyId' })
  }
  return CompanyFinancialState
}
