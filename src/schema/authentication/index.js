const { User } = require('@models')

const levelRoles = {
  // level: [roles included to this level]
  admin: ['admin'],
  seller: ['admin', 'seller'],
  user: ['admin', 'seller', 'user'] // It grants access to admins, sellers and users to the `user` level
}

const hasNeededRoles = (requiredLevel, userRoles = []) => {
  let hasNeededRoles = false
  const neededRoles = levelRoles[requiredLevel]
  if (neededRoles) {
    for (let i = 0; i < userRoles.length; i++) {
      hasNeededRoles = hasNeededRoles || neededRoles.includes(userRoles[i])
      if (hasNeededRoles) break
    }
  } else {
    throw new Error(`Unknown access level was required - ${requiresLevel}!`)
  }

  return hasNeededRoles
}

const requiresLevel = accessLevel => resolver => async (parent, args, context, info) => {
  if (context && context.user) {
    const user = await User.findOne({ where: { id: context.user.id } })
    if (user) {
      const roles = await user.getRoles()
      if (hasNeededRoles(accessLevel, roles.map(role => role.name))) {
        return resolver(parent, args, context, info)
      } else {
        throw new Error('You don\'t have needed access rights!')
      }
    } else {
      throw new Error('Access denied - unknown user!')
    }
  } else {
    throw new Error('Unauthorized')
  }
}

module.exports.requiresUserAccess = requiresLevel('user')
module.exports.requiresSellerAccess = requiresLevel('seller')
module.exports.requiresAdminAccess = requiresLevel('admin')
