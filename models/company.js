'use strict'
module.exports = (sequelize, DataTypes) => {
  const Company = sequelize.define('Company', {
    taxNumber: DataTypes.STRING,
    name: DataTypes.STRING,
    fullName: DataTypes.STRING,
    description: DataTypes.STRING,
    isBlocked: DataTypes.BOOLEAN,
    openDate: DataTypes.DATE
  }, {
    timestamps: false,
    schema: 'public'
  })
  Company.associate = (models) => {
    // associations can be defined here
    Company.belongsToMany(models.Person, { as: 'people', through: 'CompaniesPeople', foreignKey: 'companyId' })
    Company.hasMany(models.CompanyConviction, { as: 'convictions', foreignKey: 'companyId' })
    Company.hasMany(models.CompanyFinancialState, { as: 'financialStates', foreignKey: 'companyId' })
    Company.hasMany(models.CompanyTaxInfo, { as: 'taxesInfo', foreignKey: 'companyId' })
    Company.hasMany(models.TradeOffer, { as: 'tradeOffers', foreignKey: 'companyId' })
    Company.belongsToMany(models.Address, { as: 'addresses', through: 'CompaniesAddresses', foreignKey: 'companyId' })
  }
  return Company
}
