const env = process.env.NODE_ENV || 'development'

const settings = {
  port: process.env.PORT || 5100,
  jwtSecret: process.env.JWT_SECRET || 'somesuperdupersecret',

  isProduction: env === 'production',
  isDevelopment: env === 'development',
  isTest: env === 'test'
}

module.exports = settings
