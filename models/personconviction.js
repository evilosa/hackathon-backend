'use strict'
module.exports = (sequelize, DataTypes) => {
  const PersonConviction = sequelize.define('PersonConviction', {
    description: DataTypes.STRING
  }, {
    schema: 'public'
  })
  PersonConviction.associate = (models) => {
    // associations can be defined here
    PersonConviction.belongsTo(models.Person, { as: 'person', foreignKey: 'personId' })
  }
  return PersonConviction
}
