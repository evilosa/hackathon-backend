const { GraphQLList } = require('graphql')
const { Trade, Company, TradeOffer } = require('@models')
const TradeType = require('@schema/types/tradeType')

const resolve = async (parentValue, args) => {
  return Trade.findAll({ include: [ { model: TradeOffer, as: 'offers', include: [ { model: Company, as: 'company' } ] } ] })
}

const trades = {
  type: new GraphQLList(TradeType),
  resolve
}

module.exports = trades
module.exports.getTradesAsync = resolve
