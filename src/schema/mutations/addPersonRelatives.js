const graphql = require('graphql')

const { GraphQLNonNull, GraphQLList, GraphQLBoolean, GraphQLString } = graphql
const { Person } = require('../../../models')

const resolve = async (_parent, { taxNumber, relatives }) => {
  let isSaved = false

  const person = await Person.findOne({ where: { taxNumber } })
  if (person) {
    for (let i = 0; i < relatives.length; i++) {
      const relative = await Person.findOne({ where: { taxNumber: relatives[i] } })
      await person.addMyRelative(relative)
    }
    isSaved = true
  } else {
    throw new Error('Person with this tax number doesn\'t exist')
  }

  return isSaved
}

const addPersonRelatives = {
  type: GraphQLBoolean,
  args: {
    taxNumber: { type: new GraphQLNonNull(GraphQLString) },
    relatives: { type: new GraphQLList(new GraphQLNonNull(GraphQLString)) }
  },
  resolve
}

module.exports = addPersonRelatives
module.exports.addPersonRelativesAsync = resolve
