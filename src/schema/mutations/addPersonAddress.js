const graphql = require('graphql')

const { GraphQLNonNull, GraphQLBoolean, GraphQLString } = graphql
const { Person } = require('../../../models')
const { upsertAddressInfoAsync } = require('./upsertAddressInfo')

const resolve = async (_parent, { taxNumber, country, city, street, building, place }) => {
  let isSaved = false

  const person = await Person.findOne({ where: { taxNumber } })
  if (person) {
    const address = await upsertAddressInfoAsync(null, { country, city, street, building, place })
    await person.addAddress(address)
    isSaved = true
  } else {
    throw new Error('Person with this tax number doesn\'t exist')
  }

  return isSaved
}

const addPersonAddress = {
  type: GraphQLBoolean,
  args: {
    taxNumber: { type: new GraphQLNonNull(GraphQLString) },
    country: { type: new GraphQLNonNull(GraphQLString) },
    city: { type: new GraphQLNonNull(GraphQLString) },
    street: { type: new GraphQLNonNull(GraphQLString) },
    building: { type: new GraphQLNonNull(GraphQLString) },
    place: { type: GraphQLString }
  },
  resolve
}

module.exports = addPersonAddress
module.exports.addPersonAddressAsync = resolve
