const { GraphQLEnumType } = require('graphql')

const answerStatus = {
  ok: 'ok',
  notFound: 'not-found',
  error: 'error'
}

const AnswerStatusEnum = new GraphQLEnumType({
  name: 'AnswerStatus',
  values: {
    OK: { value: answerStatus.ok },
    NOT_FOUND: { value: answerStatus.notFound },
    ERROR: { value: answerStatus.error }
  }
})

module.exports = AnswerStatusEnum
module.exports.answerStatus = answerStatus
