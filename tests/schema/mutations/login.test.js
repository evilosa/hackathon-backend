const { User, Role } = require('@models')
const { loginAsync } = require('@schema/mutations/login')
const { signUpAsync } = require('@schema/mutations/signUp')

describe('Login mutation', () => {
  beforeAll(async () => {
    await User.destroy({ truncate: true, cascade: true })
    await Role.destroy({ truncate: true, cascade: true })
  })

  const loginData = { email: 'email@email.com', password: 'password' }

  it('should allow exist user to login', async () => {
    await signUpAsync(null, loginData)
    const token = await loginAsync(null, loginData)
    expect(token).toBeDefined()
  })

  describe('throw error checks', () => {
    it('should throw wrong email/password error for wrong email', async () => {
      await expect(loginAsync(null, { email: 'dsdf@dfsd.com', password: 'password' })).rejects.toThrowError('Wrong email or password!')
    })

    it('should throw wrong email/password error for wrong password', async () => {
      await expect(loginAsync(null, { email: 'email@email.com', password: 'password1' })).rejects.toThrowError('Wrong email or password!')
    })
  })
})
