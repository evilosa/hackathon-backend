'use strict'

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      { tableName: 'People', schema: 'public' },
      [{
        id: 1,
        taxNumber: '2368002232221',
        name: 'Ivan',
        middleName: '',
        surname: ''
      },
      {
        id: 2,
        taxNumber: '2368056562221',
        name: 'Aleksei',
        middleName: '',
        surname: ''
      },
      {
        id: 3,
        taxNumber: '2368045654651',
        name: 'Kaare',
        middleName: '',
        surname: ''
      },
      {
        id: 4,
        taxNumber: '236802332221',
        name: 'Anders',
        middleName: '',
        surname: ''
      },
      {
        id: 5,
        taxNumber: '23680029886221',
        name: 'Michele',
        middleName: '',
        surname: ''
      },
      {
        id: 6,
        taxNumber: '2361232232221',
        name: 'Kuzma',
        middleName: '',
        surname: ''
      },
      {
        id: 7,
        taxNumber: '2368343232221',
        name: 'Petr',
        middleName: '',
        surname: ''
      },
      {
        id: 8,
        taxNumber: '2434002232221',
        name: 'Alina',
        middleName: '',
        surname: ''
      },
      {
        id: 9,
        taxNumber: '25764232221',
        name: 'Fekla',
        middleName: '',
        surname: ''
      },
      {
        id: 10,
        taxNumber: '2355442221',
        name: 'Vasilii',
        middleName: '',
        surname: ''
      }
      ],
      {}
    )
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('People', null, {})
  }
}
