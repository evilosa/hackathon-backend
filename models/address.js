'use strict'
module.exports = (sequelize, DataTypes) => {
  const Address = sequelize.define('Address', {
    country: DataTypes.STRING,
    city: DataTypes.STRING,
    street: DataTypes.STRING,
    building: DataTypes.STRING,
    place: DataTypes.STRING
  }, {
    timestamps: false,
    schema: 'public'
  })
  Address.associate = (models) => {
    // associations can be defined here
    Address.belongsToMany(models.Person, { as: 'people', through: 'PeopleAddresses', foreignKey: 'addressId' })
    Address.belongsToMany(models.Company, { as: 'companies', through: 'CompaniesAddresses', foreignKey: 'addressId' })
  }
  return Address
}
