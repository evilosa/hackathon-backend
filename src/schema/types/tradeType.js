const graphql = require('graphql')

const { GraphQLObjectType, GraphQLID, GraphQLList, GraphQLString } = graphql
const TradeOfferType = require('./tradeOfferType')

const TradeType = new GraphQLObjectType({
  name: 'Trade',
  fields: () => ({
    id: { type: GraphQLID },
    title: { type: GraphQLString },
    description: { type: GraphQLString },
    offers: { type: new GraphQLList(TradeOfferType) }
  })
})

module.exports = TradeType
