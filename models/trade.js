'use strict'
module.exports = (sequelize, DataTypes) => {
  const Trade = sequelize.define('Trade', {
    title: DataTypes.STRING,
    description: DataTypes.STRING
  }, {
    schema: 'public'
  })
  Trade.associate = (models) => {
    // associations can be defined here
    Trade.hasMany(models.TradeOffer, { as: 'offers', foreignKey: 'tradeId' })
  }
  return Trade
}
