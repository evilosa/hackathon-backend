const { Company, Address } = require('@models')

const checkCompanyAddressesAffilationAsync = async (companies) => {
  const listOfCompanies = await Company.findAll({ where: { id: companies }, include: { model: Address, as: 'addresses', foreignKey: 'addressId' }, orderBy: 'id' })

  const affilated = {}

  for (let i = 0; i < listOfCompanies.length; i++) {
    const companyId = listOfCompanies[i].id

    let affilatedWith = []
    for (let j = 0; j < listOfCompanies.length; j++) {
      if (i !== j) {
        const intersection = listOfCompanies[i].addresses.filter(address => listOfCompanies[j].addresses.map(addr => addr.id).includes(address.id))
        if (intersection.length > 0) {
          affilatedWith.push(`Поставщиком ${j + 1}`)
        }
      }
    }

    affilated[companyId] = affilatedWith.map(affilatedId => ({
      marker: 'Совпадение адреса компаний',
      level: 'warning',
      result: `Связан с ${affilatedId}`
    })) || []
  }

  return affilated
}

module.exports = checkCompanyAddressesAffilationAsync
