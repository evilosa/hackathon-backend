module.exports = {
  automock: false,
  testEnvironment: 'node',
  setupFiles: [
    './tests/setup_tests_env.js'
  ],
  setupFilesAfterEnv: [
    './tests/setup_tests.js'
  ],
  maxConcurrency: 1,
  moduleNameMapper: {
    '^@tests(.*)$': '<rootDir>/tests$1',
    '^@config(.*)$': '<rootDir>/config$1',
    '^@models(.*)$': '<rootDir>/models$1',
    '^@schema(.*)$': '<rootDir>/src/schema$1',
    '^@app(.*)$': '<rootDir>/src$1',
    '^@root': '<rootDir>$1'
  }
}
