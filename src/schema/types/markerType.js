const graphql = require('graphql')

const { GraphQLObjectType, GraphQLString } = graphql

const AnalyseType = new GraphQLObjectType({
  name: 'Analyse',
  fields: () => ({
    marker: { type: GraphQLString },
    level: { type: GraphQLString },
    result: { type: GraphQLString }
  })
})

module.exports = AnalyseType
