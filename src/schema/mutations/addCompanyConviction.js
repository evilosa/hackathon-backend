const graphql = require('graphql')

const { GraphQLNonNull, GraphQLBoolean, GraphQLString } = graphql
const { Company, CompanyConviction } = require('../../../models')

const resolve = async (_parent, { taxNumber, description }) => {
  let isSaved = false

  const company = await Company.findOne({ where: { taxNumber } })
  if (company) {
    await CompanyConviction.create({ companyId: company.id, description })
    isSaved = true
  } else {
    throw new Error('Company with this tax number doesn\'t exist')
  }

  return isSaved
}

const addCompanyConviction = {
  type: GraphQLBoolean,
  args: {
    taxNumber: { type: new GraphQLNonNull(GraphQLString) },
    description: { type: new GraphQLNonNull(GraphQLString) }
  },
  resolve
}

module.exports = addCompanyConviction
module.exports.addCompanyConvictionAsync = resolve
