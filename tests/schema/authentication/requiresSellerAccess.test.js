const { User } = require('@models')
const { getUserRoleAsync } = require('@tests/helpers/getUserRole')
const { requiresSellerAccess } = require('@schema/authentication')

describe('User authentication - requiresSellerAccess', () => {
  let user

  beforeEach(async () => {
    await User.destroy({ truncate: true, cascade: true })
    user = await User.create({ username: 'test-user', email: 'test@user.email', password: 'test-pass' })
  })

  it('should throw error for unauthorized user', async () => {
    const resolveMock = jest.fn()
    await expect(requiresSellerAccess(resolveMock)(null, null, null)).rejects.toThrowError('Unauthorized')
  })

  it('should throw error for unknown user', async () => {
    const resolveMock = jest.fn()
    await expect(requiresSellerAccess(resolveMock)('parent', 'args', { user: { id: -1 } }, 'info')).rejects.toThrowError('Access denied - unknown user!')
  })

  it('should throw error for user withour needed access level', async () => {
    const resolveMock = jest.fn()
    await expect(requiresSellerAccess(resolveMock)('parent', 'args', { user: { id: user.id, username: user.username } }, 'info')).rejects.toThrowError('You don\'t have needed access rights!')
  })

  it('should throw error for common user', async () => {
    await user.addRole(await getUserRoleAsync('user'))
    const resolveMock = jest.fn()
    await expect(requiresSellerAccess(resolveMock)('parent', 'args', { user: { id: user.id, username: user.username } }, 'info')).rejects.toThrowError('You don\'t have needed access rights!')
  })

  it('should authenticate seller and call resolve function with correct params', async () => {
    await user.addRole(await getUserRoleAsync('seller'))
    const resolveMock = jest.fn()
    await requiresSellerAccess(resolveMock)('parent', 'args', { user: { id: user.id, username: user.username } }, 'info')
    expect(resolveMock).toHaveBeenCalled()
    expect(resolveMock.mock.calls[0][0]).toEqual('parent')
    expect(resolveMock.mock.calls[0][1]).toEqual('args')
    expect(resolveMock.mock.calls[0][2]).toEqual({ user: { id: user.id, username: user.username } })
    expect(resolveMock.mock.calls[0][3]).toEqual('info')
  })

  it('should authenticate admin with a seller access level', async () => {
    await user.addRole(await getUserRoleAsync('admin'))
    const resolveMock = jest.fn()
    await requiresSellerAccess(resolveMock)(null, null, { user: { id: user.id, username: user.username } }, null)
    expect(resolveMock).toHaveBeenCalled()
  })
})
