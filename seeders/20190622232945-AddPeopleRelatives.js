'use strict'

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      { tableName: 'PeopleRelatives', schema: 'public' },
      [
        { personId: 1, relativeId: 5 },
        { personId: 2, relativeId: 7 },
        { personId: 3, relativeId: 9 }
      ],
      {}
    )
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('PeopleRelatives', null, {})
  }
}
