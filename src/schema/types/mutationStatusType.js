const graphql = require('graphql')

const { GraphQLObjectType, GraphQLString } = graphql
const AnswerStatusEnum = require('@schema/types/apiAnswerStatusEnum')

const MutationStatusType = new GraphQLObjectType({
  name: 'MutationStatus',
  fields: () => ({
    status: { type: AnswerStatusEnum },
    error: { type: GraphQLString }
  })
})

module.exports = MutationStatusType
