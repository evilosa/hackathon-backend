const { Company, Person } = require('@models')

const getWrongOkvedMarkers = async (companies) => {
  const listOfCompanies = await Company.findAll({ where: { id: companies }, include: { model: Person, as: 'people', foreignKey: 'personId' }, orderBy: 'id' })

  const affilated = {}

  for (let i = 0; i < listOfCompanies.length; i++) {
    const companyId = listOfCompanies[i].id

    let wrongOkved = []

    if (i % 2 === 0) {
      wrongOkved.push(`Поставщика ${i + 1}`)
    }

    affilated[companyId] = wrongOkved.map(affilatedId => ({
      marker: 'Отсутствие ОКВЭД',
      level: 'error',
      result: `Отсутствие нужного вида деятельности у ${affilatedId}`
    })) || []
  }

  return affilated
}

module.exports = getWrongOkvedMarkers
