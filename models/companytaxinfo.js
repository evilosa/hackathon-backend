'use strict'
module.exports = (sequelize, DataTypes) => {
  const CompanyTaxInfo = sequelize.define('CompanyTaxInfo', {
    budget: DataTypes.STRING,
    sum: DataTypes.FLOAT
  }, {
    schema: 'public'
  })
  CompanyTaxInfo.associate = (models) => {
    // associations can be defined here
    CompanyTaxInfo.belongsTo(models.Person, { as: 'company', foreignKey: 'companyId' })
  }
  return CompanyTaxInfo
}
