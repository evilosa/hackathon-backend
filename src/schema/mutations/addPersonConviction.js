const graphql = require('graphql')

const { GraphQLNonNull, GraphQLBoolean, GraphQLString } = graphql
const { Person, PersonConviction } = require('../../../models')

const resolve = async (_parent, { taxNumber, description }) => {
  let isSaved = false

  const person = await Person.findOne({ where: { taxNumber } })
  if (person) {
    await PersonConviction.create({ personId: person.id, description })
    isSaved = true
  } else {
    throw new Error('Person with this tax number doesn\'t exist')
  }

  return isSaved
}

const addPersonConviction = {
  type: GraphQLBoolean,
  args: {
    taxNumber: { type: new GraphQLNonNull(GraphQLString) },
    description: { type: new GraphQLNonNull(GraphQLString) }
  },
  resolve
}

module.exports = addPersonConviction
module.exports.addPersonConvictionAsync = resolve
