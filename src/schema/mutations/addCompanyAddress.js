const graphql = require('graphql')

const { GraphQLNonNull, GraphQLBoolean, GraphQLString } = graphql
const { Company } = require('../../../models')
const { upsertAddressInfoAsync } = require('./upsertAddressInfo')

const resolve = async (_parent, { taxNumber, country, city, street, building, place }) => {
  let isSaved = false

  const company = await Company.findOne({ where: { taxNumber } })
  if (company) {
    const address = await upsertAddressInfoAsync(null, { country, city, street, building, place })
    await company.addAddress(address)
    isSaved = true
  } else {
    throw new Error('Company with this tax number doesn\'t exist')
  }

  return isSaved
}

const addCompanyAddress = {
  type: GraphQLBoolean,
  args: {
    taxNumber: { type: new GraphQLNonNull(GraphQLString) },
    country: { type: new GraphQLNonNull(GraphQLString) },
    city: { type: new GraphQLNonNull(GraphQLString) },
    street: { type: new GraphQLNonNull(GraphQLString) },
    building: { type: new GraphQLNonNull(GraphQLString) },
    place: { type: GraphQLString }
  },
  resolve
}

module.exports = addCompanyAddress
module.exports.addCompanyAddressAsync = resolve
