const graphql = require('graphql')

const { GraphQLNonNull, GraphQLString } = graphql
const { Trade } = require('../../../models')

const resolve = async (_parent, args) => {
  const trade = await Trade.create(args)
  return trade.id
}

const addTrade = {
  type: GraphQLString,
  args: {
    title: { type: new GraphQLNonNull(GraphQLString) },
    description: { type: new GraphQLNonNull(GraphQLString) }
  },
  resolve
}

module.exports = addTrade
module.exports.addTradeAsync = resolve
