const graphql = require('graphql')
const { GraphQLScalarType } = graphql

const isValidDate = dateObj => !isNaN(dateObj.getTime())
const DateTimeType = new GraphQLScalarType({
  name: 'DateTime',
  description: 'Represents a date with time',
  serialize (dateObj) {
    if (typeof dateObj === 'string') {
      return dateObj
    }
    if (isValidDate(dateObj)) return dateObj.toJSON()
    throw new TypeError('Date cannot represent an invalid Date instance')
  },

  parseValue (dateString) {
    if (typeof dateString === 'string') {
      const dateObj = new Date(dateString)
      if (isValidDate(dateObj)) return dateObj
      throw new TypeError('Date cannot represent an invalid Date instance')
    }
    throw new TypeError('It is not a string')
  },

  parseLiteral (ast) {
    if (ast.kind === Kind.STRING) return this.parseValue(ast.value)
    throw new TypeError('It is not a string')
  },
})

module.exports = {
  DateTimeType
}
